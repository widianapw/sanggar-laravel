<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sanggar extends Model
{
    protected $table = "sanggars";
    protected $guarded = [];

    public function jamOperasional()
    {
        return $this->hasMany(JamOperasional::class, 'sanggar_id');
    }

    public function jadwalSanggar()
    {
        return $this->hasMany(JadwalSanggar::class, 'sanggar_id');
    }
}
