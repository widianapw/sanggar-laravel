<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FasilitasSanggar extends Model
{
    use SoftDeletes;
    protected $table = "fasilitas_sanggars";
    protected $guarded = [];

    public function sanggar()
    {
        return $this->belongsto(sanggar::class, 'sanggar_id');
    }

    public function scopeOwnership($query)
    {
        return $query->where('sanggar_id', '=', auth()->user()->sanggar_id);
    }
}
