<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kegiatan extends Model
{
    use SoftDeletes;
    protected $table = "kegiatans";
    protected $guarded = [];

    public function sanggar()
    {
        return $this->belongsto(sanggar::class, 'sanggar_id');
    }
}
