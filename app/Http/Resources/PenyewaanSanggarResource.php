<?php

namespace App\Http\Resources;

use App\PenyewaanSanggar;
use Illuminate\Http\Resources\Json\JsonResource;

class PenyewaanSanggarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'sanggar' => new SanggarResource($this->sanggar),
            'tanggal' => $this->tanggal,
            'jam_mulai' => $this->jam_mulai,
            'jam_selesai' => $this->jam_selesai,
            'metode_pembayaran' => PenyewaanSanggar::getMetodePembayaran($this->metode_pembayaran),
            'bukti_pembayaran' => url('images/bukti_pembayaran/' . $this->bukti_pembayaran),
            'status' => PenyewaanSanggar::getPenyewaanStatus($this->status)
        ];
    }
}
