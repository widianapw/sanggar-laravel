<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SanggarDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'alamat' => $this->alamat,
            'telepon' => $this->telepon,
            'bank' => $this->bank,
            'nomor_rekening' => $this->nomor_rekening,
            'foto_profil' => $this->foto_profil,
            'harga_pendaftaran_siswa' => $this->harga_pendaftaran_siswa,
            'harga_penyewaan_siswa' => $this->harga_penyewaan_perjam,
            'jam_operasional' => $this->jamOperasional,
            'jadwal_sanggar' => $this->jadwalSanggar
        ];
    }
}
