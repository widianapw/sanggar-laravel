<?php

namespace App\Http\Resources;

use App\PendaftaranSiswa;
use Illuminate\Http\Resources\Json\JsonResource;

class PendaftaranSiswaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'anak' => new AnakResource($this->anak),
            'sanggar' => new SanggarResource($this->sanggar),
            'bukti_pembayaran' => url('images/pendaftaran_siswa/' . $this->bukti_pembayaran),
            'status' => PendaftaranSiswa::getPendaftaranStatus($this->status)
        ];
    }
}
