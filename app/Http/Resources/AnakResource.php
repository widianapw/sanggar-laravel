<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AnakResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'umur' => $this->umur,
            'tanggal_lahir' => $this->tanggal_lahir,
            'telepon' => $this->telepon,
            'user' => new UserResource($this->user)
        ];
    }
}
