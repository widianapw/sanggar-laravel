<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'accessToken' => $this->access_token,
            'fcmToken' => $this->fcm_token,
            'email' => $this->email,
            'username' => $this->username,
            'photoUrl' => url('/img/profile') . '/' . $this->foto_profil,
            'phone' => $this->telepon,
            'sanggar' => new SanggarDetailResource($this->sanggar)
        ];
    }
}
