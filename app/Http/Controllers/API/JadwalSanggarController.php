<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\JadwalSanggarRequest;
use App\Http\Resources\JadwalSanggarResource;
use App\Http\Resources\Success;
use App\JadwalSanggar;
use Illuminate\Http\Request;

class JadwalSanggarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwalSanggar = JadwalSanggar::where('sanggar_id', auth()->user()->sanggar_id)->orderBy('hari', 'asc')->orderBy('jam_mulai', 'asc')->get();
        return JadwalSanggarResource::collection($jadwalSanggar);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JadwalSanggarRequest $request)
    {
        $request['sanggar_id'] = auth()->user()->sanggar_id;
        $sanggar = JadwalSanggar::updateOrCreate($request->except(['_token']));

        return new JadwalSanggarResource($sanggar);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JadwalSanggar  $jadwalSanggar
     * @return \Illuminate\Http\Response
     */
    public function show(JadwalSanggar $jadwalSanggar)
    {
        return new JadwalSanggarResource($jadwalSanggar);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JadwalSanggar  $jadwalSanggar
     * @return \Illuminate\Http\Response
     */
    public function edit(JadwalSanggar $jadwalSanggar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JadwalSanggar  $jadwalSanggar
     * @return \Illuminate\Http\Response
     */
    public function update(JadwalSanggarRequest $request, JadwalSanggar $jadwalSanggar)
    {
        $jadwalSanggar->update($request->except(['_token']));
        return new JadwalSanggarResource($jadwalSanggar);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JadwalSanggar  $jadwalSanggar
     * @return \Illuminate\Http\Response
     */
    public function destroy(JadwalSanggar $jadwalSanggar)
    {
        $jadwalSanggar->delete();
        return new Success([]);
    }
}
