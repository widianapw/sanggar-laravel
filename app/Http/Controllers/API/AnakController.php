<?php

namespace App\Http\Controllers\API;

use App\Anak;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnakRequest;
use App\Http\Resources\AnakResource;
use App\Http\Resources\Success;
use Illuminate\Http\Request;

class AnakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Anak::where('user_id', auth()->user()->id)->latest()->get();
        return AnakResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnakRequest $request)
    {
        $request['user_id'] = auth()->user()->id;
        $data = Anak::updateOrCreate($request->except(['_token']));
        return new AnakResource($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Anak $anak)
    {
        return new AnakResource($anak);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnakRequest $request, Anak $anak)
    {
        $anak->update($request->all());
        return new AnakResource($anak);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Anak $anak)
    {
        $anak->delete();
        return new Success([]);
    }
}
