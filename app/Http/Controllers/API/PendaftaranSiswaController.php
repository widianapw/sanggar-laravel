<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PendaftaranSiswaRequest;
use App\Http\Resources\PendaftaranSiswaResource;
use App\Http\Resources\Success;
use App\PendaftaranSiswa;
use Illuminate\Http\Request;

class PendaftaranSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PendaftaranSiswa::where('sanggar_id', auth()->user()->sanggar_id)->whereHas('anak', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->get();
        return PendaftaranSiswaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PendaftaranSiswaRequest $request)
    {
        $request['sanggar_id'] = auth()->user()->sanggar_id;
        $pendaftaranSiswa = PendaftaranSiswa::updateOrCreate($request->except(['_token']));
        return new PendaftaranSiswaResource($pendaftaranSiswa);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PendaftaranSiswa $pendaftaranSiswa)
    {
        return new PendaftaranSiswaResource($pendaftaranSiswa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PendaftaranSiswa $pendaftaranSiswa)
    {
        $pendaftaranSiswa->status = $request->status;
        $pendaftaranSiswa->save();
        return new PendaftaranSiswaResource($pendaftaranSiswa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PendaftaranSiswa $pendaftaranSiswa)
    {
        $pendaftaranSiswa->delete();
        return new Success([]);
    }

    public function updateImage(Request $request, PendaftaranSiswa $pendaftaranSiswa)
    {
        $image = $request->file('image');
        $name = uniqid() . '.png';
        $image->move(public_path() . '/images/pendaftaran_siswa/', $name);
        $pendaftaranSiswa->bukti_pembayaran = $name;
        $pendaftaranSiswa->save();
        return new PendaftaranSiswaResource($pendaftaranSiswa);
    }
}
