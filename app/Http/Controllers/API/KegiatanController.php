<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\KegiatanRequest;
use App\Http\Resources\KegiatanResource;
use App\Http\Resources\SanggarResource;
use App\Http\Resources\Success;
use App\Kegiatan;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kegiatan = Kegiatan::where('sanggar_id', auth()->user()->sanggar_id)->latest()->get();
        return KegiatanResource::collection($kegiatan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KegiatanRequest $request)
    {
        $request['sanggar_id'] = auth()->user()->sanggar_id;
        $kegiatan = Kegiatan::create($request->except(['_token', 'image']));
        return new KegiatanResource($kegiatan);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function show(Kegiatan $kegiatan)
    {
        return new KegiatanResource($kegiatan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit(Kegiatan $kegiatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(KegiatanRequest $request, Kegiatan $kegiatan)
    {
        // $request['sanggar_id'] = auth()->user()->sanggar_id;
        $kegiatan->update($request->except(['_token', 'image']));
        return new KegiatanResource($kegiatan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kegiatan $kegiatan)
    {
        $kegiatan->delete();
        return new Success([]);
    }

    public function updateImage(Request $request, Kegiatan $kegiatan)
    {

        $image = $request->file('image');
        $name = uniqid() . '.png';
        $image->move(public_path() . '/images/kegiatan/', $name);
        $kegiatan->foto = $name;
        $kegiatan->save();
        return new KegiatanResource($kegiatan);
    }
}
