<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PenyewaanSanggarRequest;
use App\Http\Resources\PenyewaanSanggarResource;
use App\PenyewaanSanggar;
use Illuminate\Http\Request;

class PenyewaanSanggarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = PenyewaanSanggar::where('sanggar_id', auth()->user()->sanggar_id)->latest()->get();
        if ($request->has('tanggal')) {
            $data = PenyewaanSanggar::where('sanggar_id', auth()->user()->sanggar_id)->where('tanggal', $request->tanggal)->latest()->get();
        }
        return PenyewaanSanggarResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenyewaanSanggarRequest $request)
    {
        $request['user_id'] = auth()->user()->id;
        $request['sanggar_id'] = auth()->user()->sanggar_id;
        $request['status'] = "0";
        $penyewaanSanggar = PenyewaanSanggar::updateOrCreate($request->except(['_token']));
        return new PenyewaanSanggarResource($penyewaanSanggar);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PenyewaanSanggar $penyewaanSanggar)
    {
        return new PenyewaanSanggarResource($penyewaanSanggar);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PenyewaanSanggar $penyewaanSanggar)
    {
        $penyewaanSanggar->status = $request->status;
        $penyewaanSanggar->save();
        return new PenyewaanSanggarResource($penyewaanSanggar);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PenyewaanSanggar $penyewaanSanggar)
    {
        //
    }

    public function updateImage(Request $request, PenyewaanSanggar $penyewaanSanggar)
    {
        $image = $request->file('image');
        $name = uniqid() . '.png';
        $image->move(public_path() . '/images/bukti_pembayaran/', $name);
        $penyewaanSanggar->bukti_pembayaran = $name;
        $penyewaanSanggar->save();
        return new PenyewaanSanggarResource($penyewaanSanggar);
    }
}
