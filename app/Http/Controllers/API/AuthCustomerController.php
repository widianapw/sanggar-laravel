<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRegisterRequest;
use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use App\Sanggar;

class AuthCustomerController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->where('role', '0')->first();
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password]) || empty($user)) {
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Error',
                    'errors' => [
                        [
                            'title' => 'Data not Matched',
                            'message' => 'Wrong email and Password'
                        ]
                    ]
                ]
            ], 422);
        } else {
            $accessToken = Auth::user()->createToken('authToken')->accessToken;
            $user->fcm_token = $request->fcm_token;
            $user->save();
            $admin = $user;
            $admin->access_token = $accessToken;
            return new UserResource($admin);
        }
    }

    public function register(CustomerRegisterRequest $request)
    {
        // return $request;
        $user = new User;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role = "0";
        $user->fcm_token = $request->fcm_token;
        $user->password = bcrypt($request->password);
        $user->telepon = $request->telepon;
        $user->sanggar_id = $request->sanggar_id;
        $user->save();
        $user->access_token = $user->createToken('authToken')->accessToken;
        return new UserResource($user);
    }
}
