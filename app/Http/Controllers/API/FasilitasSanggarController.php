<?php

namespace App\Http\Controllers\API;

use App\FasilitasSanggar;
use Illuminate\Http\Request;
use App\Http\Resources\Success;
use App\Http\Controllers\Controller;
use App\Http\Requests\FasilitasRequest;
use App\Http\Resources\FasilitasResource;

class FasilitasSanggarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FasilitasSanggar::ownership()->get();
        return FasilitasResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FasilitasRequest $request)
    {
        $request['sanggar_id'] = auth()->user()->sanggar_id;
        $data = FasilitasSanggar::create($request->except(['_token', 'image']));
        return new FasilitasResource($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FasilitasSanggar  $fasilitasSanggar
     * @return \Illuminate\Http\Response
     */
    public function show(FasilitasSanggar $fasilitasSanggar)
    {
        return new FasilitasResource($fasilitasSanggar);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FasilitasSanggar  $fasilitasSanggar
     * @return \Illuminate\Http\Response
     */
    public function edit(FasilitasSanggar $fasilitasSanggar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FasilitasSanggar  $fasilitasSanggar
     * @return \Illuminate\Http\Response
     */
    public function update(FasilitasRequest $request,  $fasilitas_sanggar)
    {
        $fasilitas = FasilitasSanggar::find($fasilitas_sanggar)->first();
        $fasilitas->update($request->except(['image']));
        return new FasilitasResource($fasilitas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FasilitasSanggar  $fasilitasSanggar
     * @return \Illuminate\Http\Response
     */
    public function destroy($fasilitasSanggar)
    {
        $fasilitas = FasilitasSanggar::find($fasilitasSanggar)->first();
        $fasilitas->delete();
        return new Success([]);
    }


    public function updateImage(Request $request, $id)
    {
        $fasilitas = FasilitasSanggar::find($id)->first();
        $image = $request->file('image');
        $name = uniqid() . '.png';
        $image->move(public_path() . '/images/fasilitas/', $name);
        $fasilitas->foto = $name;
        $fasilitas->save();
        return new FasilitasResource($fasilitas);
    }
}
