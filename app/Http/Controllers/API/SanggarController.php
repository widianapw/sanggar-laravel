<?php

namespace App\Http\Controllers\API;

use App\User;
use DateTime;
use DatePeriod;
use App\Sanggar;
use DateInterval;
use Carbon\Carbon;
use App\JamOperasional;
use App\PenyewaanSanggar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use App\Http\Requests\SanggarRequest;
use App\Http\Resources\SanggarResource;
use App\Http\Resources\SanggarDetailResource;

class SanggarController extends Controller
{
    public function index()
    {
        // $penyewaan = 

        // return $data;
        return SanggarResource::collection(Sanggar::latest()->get());
    }

    public function create()
    {
    }

    public function show(Sanggar $sanggar)
    {
        return new SanggarDetailResource($sanggar);
    }

    public function store(SanggarRequest $request)
    {
        // return $request;
        // return User::find(auth()->user()->id);
        $sanggar = Sanggar::create($request->except(['_token', 'jam_operasional']));

        // foreach ($request->jam_operasional as $item) {
        //     $data = new JamOperasional();
        //     $data->sanggar_id = $sanggar->id;
        //     $data->hari = $item["hari"];
        //     $data->jam_mulai = $item["jam_mulai"];
        //     $data->jam_selesai = $item["jam_selesai"];
        //     $data->save();
        // }
        // return $sanggar->id;
        $user = User::find(auth()->user()->id);
        $user->sanggar_id = $sanggar->id;
        $user->save();
        return new SanggarDetailResource($sanggar);
    }

    public function update(SanggarRequest $request, Sanggar $sanggar)
    {
        $sanggar->update($request->except(['jam_operasional']));
        // JamOperasional::where('sanggar_id', $sanggar->id)->delete();
        // foreach ($request->jam_operasional as $item) {
        //     $data = new JamOperasional();
        //     $data->sanggar_id = $sanggar->id;
        //     $data->hari = $item["hari"];
        //     $data->jam_mulai = $item["jam_mulai"];
        //     $data->jam_selesai = $item["jam_selesai"];
        //     $data->save();
        // }
        // auth()->user()->sanggar_id = $sanggar->id;
        // au->save();

        $user = User::find(auth()->user()->id);
        $user->sanggar_id = $sanggar->id;
        $user->save();
        return new SanggarDetailResource($sanggar);
    }

    public function destroy()
    {
    }

    public function updateImage(Request $request, Sanggar $sanggar)
    {
        $image = $request->file('image');
        $name = uniqid() . '.png';
        $image->move(public_path() . '/images/sanggar/', $name);
        $sanggar->foto_profil = $name;
        $sanggar->save();
        return new SanggarDetailResource($sanggar);
    }

    public function notBookedTime()
    {
        $jamPenyewaan = PenyewaanSanggar::where('sanggar_id', auth()->user()->sanggar_id)->whereDate('created_at', Carbon::today())->get();
        $jamDisewa = [];
        foreach ($jamPenyewaan as $item) {
            // echo $item;
            $jam_mulai = new DateTime($item->jam_mulai);
            $jam_selesai = new DateTime($item->jam_selesai);
            $periode = new DatePeriod(
                $jam_mulai,
                new DateInterval('PT1H'),
                $jam_selesai
            );
            foreach ($periode as $item1) {
                $jamDisewa[] = $item1->format("H:i");
            }
            // $diff = $jam_selesai->diff($jam_mulai)->format('%h');
            // echo $diff;
        }


        // $jam = Sanggar::first()->jamOperasional[0];
        // // return $jam;
        // $a = $jam->jam_mulai;
        // $b = $jam->jam_selesai;
        // $period = new DatePeriod(
        //     new DateTime($a),
        //     new DateInterval('PT1H'),
        //     new DateTime($b)
        // );
        // $data = array();
        // foreach ($period as $date) {
        //     $data[] = $date->format("H:i");
        // }


        // $result = array_values(array_diff_assoc($data, $jamDisewa));
        // return $result;
    }
}
