<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Sanggar;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AdminRegisterRequest;

class AuthAdminController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->where('role', '1')->first();
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password]) || empty($user)) {
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Error',
                    'errors' => [
                        [
                            'title' => 'Data not Matched',
                            'message' => 'Wrong email and Password'
                        ]
                    ]
                ]
            ], 422);
        } else {

            $user->fcm_token = $request->fcm_token;

            $user->save();
            $user->access_token = $user->createToken('authToken')->accessToken;

            return new UserResource($user);
        }
    }

    public function register(AdminRegisterRequest $request)
    {
        $sanggar = new Sanggar;
        $sanggar->nama = $request->sanggar;
        $sanggar->save();

        $user = new User;
        $user->email = $request->email;
        // $user->username = $request->username;
        $user->role = "1";
        // $user->fcm_token = $request->fcm_token;
        $user->password = bcrypt($request->password);
        $user->telepon = $request->telepon;
        $user->sanggar_id = $sanggar->id;
        $user->save();

        $user->access_token = $user->createToken('authToken')->accessToken;

        // $sanggar = new Sanggar();
        // $sanggar->name = $request->name;
        // $sanggar->save();
        return new UserResource($user);
    }
}
