<?php

namespace App\Http\Controllers\API;

use App\Sanggar;
use App\JamOperasional;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\JamOperasionalRequest;
use App\Http\Resources\JamOperasionalResource;

class JamOperasionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JamOperasionalRequest $request)
    {
        $sanggarId = auth()->user()->sanggar_id;
        JamOperasional::where('sanggar_id', $sanggarId)->delete();
        foreach ($request->jam_operasional as $item) {
            $data = new JamOperasional();
            $data->sanggar_id = $sanggarId;
            $data->hari = $item["hari"];
            $data->jam_mulai = $item["jam_mulai"];
            $data->jam_selesai = $item["jam_selesai"];
            $data->save();
        }
        $data = JamOperasional::where('sanggar_id', $sanggarId)->get();
        return JamOperasionalResource::collection($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
