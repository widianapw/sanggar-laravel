<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'umur' => 'required|numeric|min:0',
            'tanggal_lahir' => 'required',
            'telepon' => 'required|numeric'
        ];
    }
}
