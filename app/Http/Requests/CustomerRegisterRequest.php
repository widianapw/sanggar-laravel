<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'unique:users'],
            'username' => ['required'],
            'telepon' => ['required', 'numeric'],
            'password' => ['required', 'min:8'],
            'sanggar_id' => ['required', 'numeric']
        ];
    }
}
