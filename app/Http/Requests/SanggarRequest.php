<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SanggarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'alamat' => 'required|min:8',
            'bank' => 'required',
            'telepon' => 'required|numeric',
            'nomor_rekening' => 'required',
            'harga_pendaftaran_siswa' => 'required|numeric',
            'harga_penyewaan_perjam' => 'required|numeric',
            // 'jam_operasional' => 'required|array',
            // 'jam_operasional.*.hari' => 'required',
            // 'jam_operasional.*.jam_mulai' => 'required|date_format:H:i',
            // 'jam_operasional.*.jam_selesai' => 'required|date_format:H:i'
        ];
    }
}
