<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JamOperasionalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jam_operasional' => 'required|array',
            'jam_operasional.*.hari' => 'required',
            'jam_operasional.*.jam_mulai' => 'required|date_format:H:i',
            'jam_operasional.*.jam_selesai' => 'required|date_format:H:i'
        ];
    }
}
