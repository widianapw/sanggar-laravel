<?php

namespace App\Observers;

use App\Sanggar;

class SanggarObserver
{
    /**
     * Handle the sanggar "created" event.
     *
     * @param  \App\Sanggar  $sanggar
     * @return void
     */
    public function created(Sanggar $sanggar)
    {
        auth()->user()->update([
            'sanggar_id' => $sanggar->id
        ]);
    }

    /**
     * Handle the sanggar "updated" event.
     *
     * @param  \App\Sanggar  $sanggar
     * @return void
     */
    public function updated(Sanggar $sanggar)
    {
        //
    }

    /**
     * Handle the sanggar "deleted" event.
     *
     * @param  \App\Sanggar  $sanggar
     * @return void
     */
    public function deleted(Sanggar $sanggar)
    {
        //
    }

    /**
     * Handle the sanggar "restored" event.
     *
     * @param  \App\Sanggar  $sanggar
     * @return void
     */
    public function restored(Sanggar $sanggar)
    {
        //
    }

    /**
     * Handle the sanggar "force deleted" event.
     *
     * @param  \App\Sanggar  $sanggar
     * @return void
     */
    public function forceDeleted(Sanggar $sanggar)
    {
        //
    }
}
