<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendaftaranSiswa extends Model
{
    protected $table = "pendaftaran_siswas";
    protected $guarded = [];
    public function anak()
    {
        return $this->belongsTo(Anak::class, 'anak_id');
    }

    public function sanggar()
    {
        return $this->belongsTo(Sanggar::class, 'sanggar_id');
    }

    public static function getPendaftaranStatus($id)
    {
        if ($id == "0") {
            return "Menunggu Verifikasi";
        } else if ($id == "1") {
            return "Terverifikasi";
        } else {
            return "Ditolak";
        }
    }
}
