<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenyewaanSanggar extends Model
{
    protected $table = "penyewaan_sanggars";
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function sanggar()
    {
        return $this->belongsTo(Sanggar::class, 'sanggar_id');
    }

    public static function getMetodePembayaran($id)
    {
        if ($id == '0') {
            return "Transfer";
        } else {
            return "Pembayaran di tempat";
        }
    }

    public static function getPenyewaanStatus($id)
    {
        if ($id == '0') {
            return "Menunggu Konfirmasi";
        } else if ($id == '1') {
            return "Terverifikasi";
        } else {
            return "Dibatalkan";
        }
    }
}
