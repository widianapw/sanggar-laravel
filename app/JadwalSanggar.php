<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JadwalSanggar extends Model
{
    use SoftDeletes;
    protected $table = "jadwal_sanggars";
    protected $guarded = [];

    public function sanggar()
    {
        return $this->belongsTo(Sanggar::class, 'sanggar_id');
    }
}
