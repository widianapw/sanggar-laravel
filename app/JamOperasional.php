<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JamOperasional extends Model
{
    use SoftDeletes;
    protected $table = "jam_operasionals";
    protected $guarded = [];
}
