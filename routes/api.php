<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('admin')->group(function () {
    Route::post('register', 'API\AuthAdminController@register');
    Route::post('login', 'API\AuthAdminController@login');
    // Route::middleware([''])->group(function () {
    // });
    Route::middleware('auth:api')->group(function () {
        Route::resource('sanggar', 'API\SanggarController');
        Route::post('sanggar/{sanggar}/update-image', 'API\SanggarController@updateImage');
        Route::resource('jadwal-sanggar', 'API\JadwalSanggarController');
        Route::resource('kegiatan', 'API\KegiatanController');
        Route::post('kegiatan/{kegiatan}/update-image', 'API\KegiatanController@updateImage');
        Route::resource('fasilitas', 'API\FasilitasSanggarController');
        Route::post('fasilitas/{id}/update-image', 'API\FasilitasSanggarController@updateImage');
        Route::resource('penyewaan-sanggar', 'API\PenyewaanSanggarController');
        Route::resource('pendaftaran-siswa', 'API\PendaftaranSiswaController');
        Route::resource('jam-operasional', 'API\JamOperasionalController');
    });
});

Route::prefix('customer')->group(function () {
    Route::post('register', 'API\AuthCustomerController@register');
    Route::post('login', 'API\AuthCustomerController@login');
    Route::resource('sanggar', 'API\SanggarController');
    Route::middleware('auth:api')->group(function () {
        Route::resource('anak', 'API\AnakController');
        Route::resource('penyewaan-sanggar', 'API\PenyewaanSanggarController');
        Route::resource('pendaftaran-siswa', 'API\PendaftaranSiswaController');
        Route::post('pendaftaran-siswa/{pendaftaranSiswa}/update-image', 'API\PendaftaranSiswaController@updateImage');
        Route::resource('jadwal-sanggar', 'API\JadwalSanggarController');
        Route::resource('kegiatan', 'API\KegiatanController');
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
